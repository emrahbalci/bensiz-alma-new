require('flexslider');

function initSlider() {
    $('.flexslider').flexslider();
    $('.flexslider-col3').flexslider({
        animation: "slide",
        animationLoop: true,
        itemWidth: 880/3,
        minItems: 3,
        maxItems: 3
    });
}

module.exports = {
    init: initSlider
}