{
    // jquery
    let $ = global.$ || {};
    let jQuery = global.jQuery || {};

    $ = jQuery = require('jquery');

    global.$ = $;
    global.jQuery = jQuery;

    require('jquery-colorbox');
    require('jquery-validation');
    require('../../../node_modules/jquery-ui-slider/jquery-ui.js');


    const sliders = require('./vendor/flexslider.js');

    $(function() {
        sliders.init();

        // Search js
        $('.search-box input').on('focus', function () {
            $(this).parents('.search-box').addClass('focus');
            $('body').css('overflow', 'hidden');
        })
        $('.overlay').on('click', function () {
            var $this = $(this);
            setTimeout(function () {
                $('.search-box input').parents('.search-box').removeClass('focus');
                $('body').css('overflow', 'auto');
            }, 600)
        });

        window.onhashchange = function () {
            if (window.location.hash) {
                if (window.location.hash == "#giris-yap") {
                    $.colorbox.close();
                    $('[data-toggle="login"]').trigger('click');
                    $('html, body').stop().animate({scrollTop: 0}, 10);

                } else if ($(window.location.hash).length > 0) {
                    $.colorbox({inline:true, href:window.location.hash, onClosed: function () {
                        window.history.replaceState({}, "", "#");
                    }});
                }
            }
        };

        $('[data-toggle]').on('click', function () {
            $(this).toggleClass('active');
            $('[data-open="'+$(this).attr('data-toggle')+'"]').toggleClass('active');
        });

        var $requestBtn = $('#send-request');
        if ($requestBtn.length > 0) {
            $(window).on('load scroll', function () {
                var $btn = $('#send-request').offset().top;

                if (window.pageYOffset > $btn) {
                    $('.fix-request-button').addClass('active')
                } else {
                    $('.fix-request-button').removeClass('active')
                }
            });
        }
        
        // Filtre alanı
        var $filterItem = $('.filter-item'),
            selectedParams = {};
        $filterItem.each(function (index, item) {
            var $this = $(this),
                selectedValue = $this.data('selected');
            selectedParams[$this.data('qs')] = [];

            if (selectedValue && !$this.data('hasslide')) {
                var arrValue = selectedValue.split(',');
                arrValue.map(function (item) {
                    selectedParams[$this.data('qs')].push(item);
                });
            } else if (selectedValue && $this.data('hasslide')) {
                selectedParams['min-tutar'] = selectedValue[0];
                selectedParams['max-tutar'] = selectedValue[1];
            }
        });

        $(document).on('keyup',function(evt) {
            if (evt.keyCode == 27) {
                if ($filterItem.length > 0) {
                    $filterItem.find('a').removeClass('active').next('.filter-wrapper').remove();
                }
            }
        });

        function letsSearch() {
            var url = "?";

            $filterItem.each(function (index, item) {
                var qs = $(this).data('qs'),
                    selectedItem = selectedParams[$(this).data('qs')];

                if (Array.isArray(selectedItem) && selectedItem.length > 0) {
                    url += qs + '=[';

                    selectedItem.map(function (item) {
                        url+= item + ','
                    });

                    url = url.substr(0, url.length - 1);

                    url += ']&';

                    delete selectedParams[$(this).data('qs')];
                }
            });

            window.location.href= url + $.param(selectedParams);
        }

        $filterItem.find('a').on('click', function () {
            if ($(this).parent().hasClass('disabled')) {
                alert('Lütfen marka seçiniz..');
            } else if ($(this).hasClass('active')) {
                $(this).removeClass('active').next('.filter-wrapper').remove();
            } else {

                var $this = $(this).parent(),
                    hasSlide = $this.data('hasslide'),
                    data = $this.data('data'),
                    $filterWrapper = $filterItem.find('.filter-wrapper'),
                    qs = $this.data('qs'),
                    defaultText = $this.data('default-text');
                $filterWrapper.remove();
                $this.append('<div class="filter-wrapper"></div>');
                $filterWrapper = $filterItem.find('.filter-wrapper');

                $(this).toggleClass('active');

                var money = function (num) {
                    var num = num.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');

                    return num.substr(0, num.length - 3);
                }

                if (hasSlide) {
                    $filterWrapper.append('<div class="slider"></div><div class="amount"></div> ');
                    $(".slider").slider({
                        range: true,
                        min: 50000,
                        max: 1000000,
                        step: 5000,
                        values: $this.data('selected') ? $this.data('selected') : [50000, 1000000],
                        create: function (event, ui) {
                            var values = $(".slider").slider("option", "values");
                            $filterItem.find('.amount').text(money(values[0]) + " TL - " + money(values[1]) + " TL");
                        },
                        slide: function (event, ui) {
                            $filterItem.find('.amount').text("" + money(ui.values[0]) + " TL - " + money(ui.values[1]) + " TL");
                        }
                    });
                } else {
                    $filterWrapper.append('<div class="list"></div>');

                    if (qs == "model") {
                        Object.keys(data).map(function (brand, index) {
                            $filterWrapper.find('.list').append('<b>' + brand + '</b>');
                            $.each(data[brand], function (i, item) {
                                var isActive = "",
                                    objectKey = Object.keys(item),
                                    text = item[objectKey];

                                if (selectedParams[qs].length > 0 && selectedParams[qs].indexOf(objectKey[0]) > -1) {
                                    console.log(objectKey, selectedParams)
                                    isActive = "active";
                                    text += '<i class="fa fa-check"></i>';
                                }
                                $('<a>').append(text).addClass(isActive).on('click', function () {
                                    $(this).toggleClass('active');

                                    if ($(this).hasClass('active')) {
                                        selectedParams[qs].push(objectKey);
                                        $(this).prepend('<i class="fa fa-check"></i>');
                                    } else {
                                        var index = selectedParams[qs].indexOf(objectKey);
                                        if (index > -1) {
                                            selectedParams[qs].splice(index, 1);
                                        }
                                        $(this).find('i').remove();
                                    }
                                    if (selectedParams[qs].length > 0) {
                                        $this.addClass('filled').find('> a').text(selectedParams[qs].slice())
                                    } else {
                                        $this.removeClass('filled').find('> a').text(defaultText)
                                    }
                                }).appendTo($filterWrapper.find('.list'));
                            });
                        });

                    } else {
                        Object.keys(data).map(function (objectKey, index) {
                            var isActive = "",
                                text = data[objectKey];
                            if (selectedParams[qs].length > 0 && selectedParams[qs].indexOf(objectKey) > -1) {
                                isActive = "active";
                                text += '<i class="fa fa-check"></i>';
                            }
                            ;
                            $('<a>').append(text).addClass(isActive).on('click', function () {
                                $(this).toggleClass('active');

                                if ($(this).hasClass('active')) {
                                    selectedParams[qs].push(objectKey);
                                    $(this).prepend('<i class="fa fa-check"></i>');
                                } else {
                                    var index = selectedParams[qs].indexOf(objectKey);
                                    if (index > -1) {
                                        selectedParams[qs].splice(index, 1);
                                    }
                                    $(this).find('i').remove();
                                }
                                if (selectedParams[qs].length > 0) {
                                    $this.addClass('filled').find('> a').text(selectedParams[qs].slice())
                                } else {
                                    $this.removeClass('filled').find('> a').text(defaultText)
                                }
                            }).appendTo($filterWrapper.find('.list'));
                        });
                    }

                }


                $(document).on('keyup',function(evt) {
                    if (evt.keyCode == 13) {
                        if (hasSlide) {
                            var values = $(".slider").slider("option", "values");
                            selectedParams['min-tutar'] = values[0];
                            selectedParams['max-tutar'] = values[1];
                            $this.addClass('filled').find('> a').text(selectedParams['min-tutar'] ? money(selectedParams['min-tutar']) + ' TL -' + money(selectedParams['max-tutar']) + ' TL' : defaultText)
                        } else {

                        }
                        $filterItem.find('.filter-wrapper').remove();
                    }
                });

                $('<a>').addClass('apply').text('Uygula').on('click', function () {
                    if (hasSlide) {
                        var values = $(".slider").slider("option", "values");
                        selectedParams['min-tutar'] = values[0];
                        selectedParams['max-tutar'] = values[1];
                        $this.addClass('filled').find('> a').text(selectedParams['min-tutar'] ? money(selectedParams['min-tutar']) + ' TL -' + money(selectedParams['max-tutar']) + ' TL' : defaultText)
                    } else {

                    }
                    letsSearch();
                    $filterItem.find('.filter-wrapper').remove();
                }).appendTo($filterWrapper);

                $('<a>').addClass('cancel').text('İptal').on('click', function () {
                    $filterItem.find('.filter-wrapper').remove();
                }).appendTo($filterWrapper);
            }
        });

        //sonuçları görüntüle click eventi
        $('#lets-search').on('click', function () {
            letsSearch();
        });

        //temizle click eventi
        $('#reset-search').on('click', function () {
            window.location.href = '/araclar/'
        });


        //Anasayfa autocomplete
        var $searchInput = $('#searchInput'),
            timeOut;
        $searchInput.on('keyup', function (evt) {
            var key = $searchInput.val();
            if (evt.keyCode != 38 && evt.keyCode != 40)
            if (key.length > 2) {
                $('.autocomplete-list').addClass('hidden').empty();
                clearInterval(timeOut);
                timeOut = setTimeout(function () {
                    $.get(root + '/search/auto-complete/' + key + '/', function (response) {
                        for (var i = 0; i < 4; i++) {
                            if (response["product"].length > 0) {
                                $('.autocomplete-list').append('<li><a href="'+root+'/arac/'+response["product"][i]['slug'] +'">'+response["product"][i]['name']+'</a></li>').removeClass('hidden')
                            } else {
                                $('.autocomplete-list').addClass('hidden').empty();
                            }
                        }
                    });
                },300)
            }
        });

        $(document).on('keyup',function(evt) {
            var $autocomplete = $('.autocomplete-list'),
                $activeItem = $autocomplete.find('.active');
            if ($autocomplete.is(':visible')) {
                //up
                if (evt.keyCode == 38) {
                    if ($activeItem.length > 0) {
                        $activeItem.removeClass('active').prev('li').addClass('active');
                    } else {
                        $autocomplete.find('li:last-child').addClass('active');
                    }
                } else if (evt.keyCode == 40) {
                    if ($activeItem.length > 0) {
                        $activeItem.removeClass('active').next('li').addClass('active').find('a').trigger('focus');
                    } else {
                        $autocomplete.find('li:first-child').addClass('active');
                    }
                }
            }
        });


        //form validations
        $('.validate-form').each(function () {
            var $this = $(this),
                url = $this.attr('action');
            $(this).validate({
                errorPlacement: function(error, element) {
                    // Append error within linked label
                    $( element )
                        .parent().addClass('error');
                },
                submitHandler: function(form) {
                    $.post(url, $this.serializeArray(), function (res) {
                        if (res.success)
                            window.location.reload();
                        else
                            alert(res['message']);
                    })
                }

            })
        });
        
        //Ürün detay talep oluşturma
        var productSelectedParams = {};
        
        $('.color-list').on('click', function () {
            $('.color-list li').removeClass('selected');
            $(this).toggleClass('selected');

            if ($(this).hasClass('selected')) {
                productSelectedParams['color'] = $(this).data('key');
            } else {
                delete productSelectedParams['color'];
            }
        });

        $('.send-request').on('click', function (e) {
            e.preventDefault();
            var $this = $(this),
                selectedSlug = $('[name="slug"]:checked').val();

            if ($this.hasClass('notLogin')) {
                window.location.href = "#giris-redirect";
            } else {
                if (selectedSlug) {
                    productSelectedParams['slug'] = selectedSlug;
                    window.location.href = "#talep-olustur";
                } else {
                    $('html, body').stop().animate({scrollTop: 500}, 300);
                }
            }
        });

        //Talep oluşturma formu
        $('.request-form').on('submit', function (e) {
            e.preventDefault();
            var data = {
                'color': productSelectedParams['color'],
                'slug': productSelectedParams['slug'],
                'count': $('[name="quantity"]').val(),
                'message': $('[name="message"]').val()
            };

            $.post(root + '/basvuru/talep-olustur/', data, function (response) {
               if (response['status'] == "error") {
                   alert(response['message']);
               } else {
                   $('#talep-basarili b').text(response['limit']);
                   window.location.href = "#talep-basarili"
               }
            });

        });

        //talep sil
        $('.request-options-delete').on('click', function (e) {
            e.preventDefault();
            var $this = $(this);
            var r = confirm('Talebi silmek istediğinize emin misiniz?');

            if (r == true) {
                $.get(root + '/basvuru/talep-sil/' + $(this).data('key') + '/', null, function (res) {
                    if(res['status'] == "success") {
                        window.location.reload();
                    } else {
                        alert(res['message']);
                    }
                });
            }
        });

        $('.flex-list, .flex-list .container').css('min-height', $(window).height()-100);

        $('.faq-list .ask').on('click', function () {
            $('.faq-list .content').not($(this).next('.content')).slideUp();
            $(this).next('.content').slideToggle();
        })

    });
}